
$(document).ready(function()
{
    function validation0(){
    var fnameval=$("#firstname").val();
    var b=/^[a-zA-Z]{3,16}$/;
    if (fnameval.match(b)){
                $("#firstnamespa").text("");

            } 
        else {
                $("#firstnamespa").html("**Not valid, should have only charecter");
                return false;
            }
        }

function validation1(){
	var last=$("#lastname").val();
    var b=/^[a-zA-Z]{3,16}$/;
    if (last.match(b)){
                $("#lastnamespa").text("");

            } 
        else {
                $("#lastnamespa").html("**Not valid, should have only charecter");
                return false;
            }
        }


function validation2() {
        var last=$("#phone").val();
    var b=/^[+0-9]{10,13}$/;
    if (last.match(b)){
                $("#phonespa").text("");

            } 
        else {
                $("#phonespa").html("**Not valid, should have 10-12 digit");
                return false;
            }
        }


function validation3(){
            var last=$("#office").val();
    var b=/^[0-9]{10,13}$/;
    if (last.match(b)){
                $("#officespa").text("");

            } 
        else {
                $("#officespa").html("**Not valid, should have 10-12 digit");
                return false;
            }
        }

function validation4(){

    var a=$("#email").val();
    //var b=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var par=/^[a-zA-Z]+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/;
    var pattern=/^[a-zA-Z]+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}\.[a-z]{2}$/;

    // var mailformat= /^\S\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}\.[a-zA-Z]{2}$/;
    // var mailformat1= /^\S\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    // var mailformat2= /^\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    if (a.match(par) || a.match(pattern)){
                $("#emailspa").text("");

            } 
        else {
                $("#emailspa").html("**Not valid email");
                return false;
            }
        }

function validation5(){

    var last=$("#password").val();
    var b=/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,12}$/;
    if (last.match(b)){
                $("#passwordspa").text("");

            } 
        else {
                $("#passwordspa").html("**Not valid, should have alphanumeric and 8-12 digit");
                return false;
            }
        }

function validation6(){  

    var last=$("#password").val();
    var last1=$("#conpassword").val();
    if ($('#password').val() == $('#conpassword').val()) {
    $("#conpasswordspa").html("");
        return true;

    } else {
        $("#conpasswordspa").html("**Not same as password");
        return false;
    }
}


function validation7(e){
    var last=$("#about").val();
// $("#about").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
}


function validation8(){
// $('#age,#year,#day,#month').on('blur keyup', function () {
    var date=new Date;
  var Month=document.getElementById('month').value;

  var Year=document.getElementById('year').value;
 
  var Day=document.getElementById('day').value;


  if((Month==('Month'))||(Year==('Year'))||(Day==('Day'))){

        document.getElementById("dobspa").innerHTML = "**Select DOB";

        return false;

    }
    else{

        var dob=new Date(Year,Month/10,Day);
var m=date.getMonth()+1;
   
var y=date.getFullYear()-1;
var q=Month-m;
var age=(y-Year)+(Math.abs(12-q)/10)
document.getElementById("age").value=age;
document.getElementById("age").disabled = true;
        document.getElementById("dobspa").innerHTML = "";
    }
  }

function validation10(){
    if($("#checkbox_sample18").is(":checked")==false && $("#checkbox_sample19").is(":checked")==false && $("#checkbox_sample20").is(":checked")==false)
            {
            document.getElementById('cx').innerHTML ="<br>**Select At least one Checkbox";
                return false;
            }
            else
            {
                document.getElementById('cx').innerHTML ="";
                
            }
}

    function validation()
    {

        if(validation0()==false ||   validation1()==false || validation2()==false ||validation3()==false ||validation4()==false ||validation5()==false ||validation6()==false ||validation7()==false || validation8()==false || validation10()==false)

            {
                return false;
            }
        
    }

    $("#firstname").on("blur keyup", validation0)
    $("#lastname").on("blur keyup", validation1)
    $("#phone").on("blur keyup", validation2)
    $("#office").on("blur keyup", validation3)
    $("#email").on("blur keyup", validation4)
    $("#password").on("blur keyup", validation5)
    $("#conpassword").on("blur keyup", validation6)
    $("#about").on("blur keypress",validation7)
    $("#about").on("blur keypress",validation8)
    $("#about").on("blur keypress",validation10)
    $("#age,#year,#day,#month").on("change", validation8)
    $("#submit").on("click", validation)


    });
