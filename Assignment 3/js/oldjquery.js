
$(document).ready(function()
{
    function validation0(){
    	$("#firstname").on("blur keyup", function() {
            if ( $(this).val().match('^[a-zA-Z]{3,16}$') ) {
                $("#firstnamespa").html("");

            } else {
                $("#firstnamespa").html("**Not valid, should have only charecter");
                return false;
            }
});
}

function validation1(){
	$("#lastname").on("blur keyup", function() {
    if ( $(this).val().match('^[a-zA-Z]{3,16}$') ) {
        $("#lastnamespa").html("");
        	return true;

    } else {
        $("#lastnamespa").html("**Not valid, should have only charecter");
        return false;
    }
});}


function validation2() {
$("#phone").on("blur keyup", function() {
    if ( $(this).val().match('^[+0-9]{10,13}$') ) {
        $("#phonespa").html("");
        return true;

    } else {
        $("#phonespa").html("**Not valid, should have 10-12 digit");
        return false;
    }
});}


function validation3(){
$("#office").on("blur keyup", function() {
    if ( $(this).val().match('^[0-9]{10,13}$') ) {
        $("#officespa").html("");
        return true;

    } else {
        $("#officespa").html("**Not valid, should have 10-12 digit");
        return false;
    }
});}

function validation4(){
$("#email").on("blur keyup", function() {
    if ( $(this).val().match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/) ) {
        $("#emailspa").html("");
        return true;

    } else {
        $("#emailspa").html("**Not valid email");
        return false;
    }
});}

function validation5(){
$("#password").on("blur keyup", function() {
    if ( $(this).val().match("^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,12}$") ) {
        $("#passwordspa").html("");
        return true;

    } else {
        $("#passwordspa").html("**Not valid, should have alphanumeric and 8-12 digit");
        return false;
    }
});}

function validation6(){  
$('#password, #conpassword').on('blur keyup', function () {
  if ($('#password').val() == $('#conpassword').val()) {
    $("#conpasswordspa").html("");
        return true;

    } else {
        $("#conpasswordspa").html("**Not same as password");
        return false;
    }
});}


function validation7(){
$("#about").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});}


function validation8(){
$('#age,#year,#day,#month').on('blur keyup', function () {
    var date=new Date;
  var Month=document.getElementById('month').value;

  var Year=document.getElementById('year').value;
 
  var Day=document.getElementById('day').value;


  if((Month==('Month'))||(Year==('Year'))||(Day==('Day'))){

        document.getElementById("dobspa").innerHTML = "**Select DOB";

        return false;

    }
    else{

        var dob=new Date(Year,Month/10,Day);
var m=date.getMonth()+1;
   
var y=date.getFullYear()-1;
var q=Month-m;
var age=(y-Year)+(Math.abs(12-q)/10)
document.getElementById("age").value=age;
document.getElementById("age").disabled = true;
        document.getElementById("dobspa").innerHTML = "";
    }
  
    
});}


function validation9(){
$('#about,#dobspa,#year,#day,#month').on('blur keyup', function () {     var
Month=document.getElementById('month').value;     var
Year=document.getElementById('year').value;     var
Day=document.getElementById('day').value;
if((Month==('Month'))||(Year==('Year'))||(Day==('Day'))){

        document.getElementById("dobspa").innerHTML = "**Select DOB";

        return false;

    }
    else{

        document.getElementById("dobspa").innerHTML = "";
    }

});}

function validation10(){
$('.checkbox').click(function (){
    var checkboxs=document.getElementsByClassName("checkbox");
    var okay=false;
    for(var i=0,l=checkboxs.length;i<l;i++)
    {
        if(checkboxs[i].checked)
        {
            okay=true;
            break;
        }
    }
    if(okay)
        {document.getElementById('cx').innerHTML ="";
            }

    else
    { document.getElementById('cx').innerHTML ="<br>**Select At least one Checkbox";
        }
});}

    function validation()
    {

        if( validation0()==false){return false;}
        if( validation1()==false){return false;}
        if( validation2()==false){return false;}
        if( validation3()==false){return false;}
        if( validation4()==false){return false;}
        if( validation5()==false){return false;}
        if( validation6()==false){return false;}
        if( validation7()==false){return false;}
        if( validation8()==false){return false;}
        if( validation9()==false){return false;}
        if( validation10()==false){return false;}
    }

    // $("#firstname").on("blur keyup", validation0())
    // $("#lastname").on("blur", validation1())
    // $("#phone").on("blur", validation2())
    // $("#office").on("blur", validation3())
    // $("#email").on("blur", validation4())
    // $("#password").on("blur", validation5())
    // $("#conpassword").on("blur", validation6())
    // $("#about").on("blur", validation7())
    // $("#age,#year,#day,#month").on("click", validation8())
    // $(".checkbox").on("click", validation9())
    $("#submit").on("click", validation())

    });


function validation(){

   if( dob2()==false || check()==false || mand()==false || phoneCheck() ==false || checkOffice()==false || checkMail()==false || checkPassword()==false || confirmPassword()==false || about()==false)
    {
        return false;
    }
     else
    {
       return true;
    }   