$(document).ready(function() {
  function validation(){

   if( dob2()==false || check()==false || mand()==false || phoneCheck() ==false || checkOffice()==false || checkMail()==false || checkPassword()==false || confirmPassword()==false || about()==false)
    {
        return false;
    }
     else
    {
       return true;
    }	
}

function dob2(){

var Month=document.getElementById('month').value;
var Year=document.getElementById('year').value;
var Day=document.getElementById('day').value;
if((Month==('Month'))||(Year==('Year'))||(Day==('Day'))){
	$("#dobMsg").css("color","red").text("Please Select DOB");
    //document.getElementById("dobMsg").innerHTML = "<font color='red'> Please select DOB</font>";
        return false;
      }
else{

//document.getElementById("dobMsg").innerHTML = "<font color='green'>DOB selected</font";
$("#dobMsg").css("color","green").text("DOB seleted");
}


}
function check(){
	var fnameval=$("#fname").val();
    var fnamelen=Number(fnameval.length);
     if(fnamelen===0)
      {
        $("#ferr").css("color","red").text("Firstname is Required");
     
      }

    else if(fnameval.match(/^[a-zA-Z]+$/))
      {
      	$("#ferr").css("color","green").text("Valid");	
      }
    else
      {
      		 $("#ferr").css("color","red").text("Please enter your firstname only in text");
          //document.getElementById("demo").innerHTML="<font color='red'>Please enter your firstname only in text</font>";
           return false;
      }
}

function mand(){
 var lnameval=$("#lname").val();
    var lnamelen=Number(lnameval.length);
     if(lnamelen===0)
       {
       //document.getElementById("demo").innerHTML="this is invalid name";
            $("#lerr").css("color","red").text("Lastname is Required");
        // document.getElementById("fname").value="";
        // document.name_fileds.fname.focus();
       }

       else if(lnameval.match(/^[a-zA-Z]+$/))
        {
          $("#lerr").css("color","green").text("Valid");
        }
   else
      {
        $("#ferr").css("color","red").text("Please enter your lastname only in text");
         return false;
      }
    }

function phoneCheck() 
      
  {
   var phn=$("#phone").val();
   var phnlen=Number(phn.length);
   var gb = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
   var reg=/^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/;
   var validReg=/^\+?[0-9-]+$/;
   var countryReg=/^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$/;
   var newReg=/^([+][9][1]|[9][1]|[0]){0,1}([7-9]{1})([0-9]{9})$/;
   if(phnlen === 0)
     {
        //document.getElementById("message").innerHTML="<font color='red'>Mandatory Field</font>";
         $("#phoneerr").css("color","red").text("Phone Number is Required");
         return false;
    }

     
   //console.log(phn);
    //var pattern = new RegExp("([^\d])\d{10}([^\d])");

   // if (pattern.test(document.getElementById('phone').value))
  
   //else if(phn.match(/^\d{10}$/))
   if (phn[0] == '+' &&( phnlen == 13 || phnlen == 12)) {
          $("#phoneerr").css("color","green").text("Valid");

        //document.getElementById("message").innerHTML="<font color='green'>Valid</font>";
   } else if (phn[0] == '+' && phnlen !== 13) {
             $("#phoneerr").css("color","red").text("Invalid");

            return false;
   } else if (phn[0] !== '+') {
      if (phnlen > 9 && phnlen < 13) {
        $("#phoneerr").css("color","green").text("Valid");

                    //document.getElementById("message").innerHTML="<font color='green'>valid</font>";
      } else {
                    $("#phoneerr").css("color","red").text("Invalid");

                    //document.getElementById("message").innerHTML="<font color='red'>invalid</font>";
                    return false;

      }
   } 
 }

 function checkOffice(){
  var ofycNum=$("#office").val();
  console.log(ofycNum);
   var numbers = /^[0-9]+$/;
   if(ofycNum.match(numbers))
      {
        console.log(ofycNum);
      
        $("#ofycerr").css("color","green").text("Valid");
    
      }
      else
      {
      $("#ofycerr").css("color","red").text("Invalid");
        return false;
      }


  }


  function checkMail() {
       var mail=$("#email").val();
       var maillen=Number(mail.length);
       var mailformat= /^\S\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}\.[a-zA-Z]{2}$/;
        var mailformat1= /^\S\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
        var mailformat2= /^\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
      if(mail.match(mailformat2) && mail.match(mailformat1)|| mail.match(mailformat))
      {
          $("#emailerr").css("color","green").text("Valid");
      }  
      else
        {
             $("#emailerr").css("color","red").text("Inalid");
            return false;
       }
      
  }

  function checkPassword(){
      var pwdGen=$("#password").val();
      var pwdlen=Number(pwdGen.length);
    var checkPwd=/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,12})$/;
    if(pwdlen===0)
    {
     $("#passworderr").css("color","red").text("Password is Required");return false;
    }
      else if(pwdGen.match(checkPwd))
     {
       $("#passworderr").css("color","green").text("Vaid");
     }
     else{
       $("#passworderr").css("color","red").text("8-12 range no special characters");
       
        return false;
     }
   }

 function confirmPassword() {
     // body...
        var password = $("#password").val();
        var passwordlen=Number(password.length);
        var confirmPassword = $("#confirmpassword").val();
        if(passwordlen===0)
        {
          $("#confirmerr").css("color","red").text(" Confirm Password is Required");return false;
        }
         else if (password != confirmPassword) {
           $("#confirmerr").css("color","red").text("Passwords do not match");
           return false;
        }
        else{
          
          $("#confirmerr").css("color","green").text("Valid");
        }
        
   }

function about()
{
    var details=$("#about").val();
    var detailslen=Number(details.length); 
     var nospace=/^\s*\s*$/;  
        if(detailslen===0)
            {
               $("#abouterr").css("color","red").text("About you is Required");return false;
        }
              //document.getElementById("aboutMsg").innerHTML="<font color='red'>Mandatory Field</font>";
            
          else if(details.match(nospace)) 
        {
          $("#abouterr").css("color","red").text("Should not be empty");
            //document.getElementById("aboutMsg").innerHTML="<font color='red'>Should not be empty</font>";
            return false;
        }      

        else
            {
              $("#abouterr").css("color","green").text("Valid");

              
            }
  
}
function gender()
 {

console.log("hi")
    // if($("#residence1").is(":checked")==false || $("#residence1").is(":checked")==false ){
    //  $("#gendererr").css("color","red").text("Select atleast one radio button");
    //       return false;
    //  }     
    // else
      $("#gendererr").css("color","green").text("Gender selected!!");


}

function checkBox()
{

  if($("#checkbox_sample18").is(":checked")==false && $("#checkbox_sample19").is(":checked")==false && $("#checkbox_sample20").is(":checked")==false){
     $("#interesterr").css("color","red").text("Select atleast one checkbox");
          return false;

  }
  else
    $("#interesterr").css("color","green").text("Interest selected!");

}

function  calc(){
    var date=new Date;
    var birthMonth=$('#month').val();
    //onsole.log(birthMonth);

    var birthYear=$('#year').val();
    //console.log(birthYear);
    var birthDay=$('#day').val();
    //console.log(birthDay);
     if((birthMonth=='Month') || (birthDay=='Day') || (birthYear=='Year'))
      {
        document.getElementById("age").value= " ";
        return false;
      }
    else{  
        var dob=new Date(birthYear,birthMonth/10,birthDay);
        var m=date.getMonth()+1;
        var y=date.getFullYear()-1;
        var q=birthMonth-m;
        var age=(y-birthYear)+(Math.abs(12-q)/10)
        //document.getElementById("age").value=age;
        $("#age").val(age);
        $("#age").attr("disabled","disabled");

    }    
    //document.getElementById("age").disabled = true;

  }

 $("#fname").blur(check);
 $("#lname").blur(mand);
 $("#phone").blur(phoneCheck);
 $("#office").blur(checkOffice);
 $("#email").blur(checkMail);
 $("#password").blur(checkPassword);
 $("#confirmpassword").blur(confirmPassword);
 $("#month,#year,#day").change(calc);
 $("#about").blur(about);
 $(".radio").click(gender);
 $(".checkbox").click(checkBox);
 $("#submit").click(validation);
 
});

 
   
  

   
   